﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceInfo.Models;
using Xunit;
using FinanceInfo.Domain.Exceptions;
using FinanceInfo.Domain.MarketValueProviders;

namespace FinanceInfo.Domain.Test.MarketValueProviders
{
    public class CompositeMarketIndexProviderTests
    {
        [Fact]
        public void GetMarketIndex_Working()
        {
            double expected = 6.7;
            var provider = new CompositeMarketIndexProvider(new IMarketValueProvider[]
                                                    {
                                                        new WorkingTestMarketIndexProvider(expected),
                                                        new NonWorkingTestMarketIndexProvider()
                                                    });

            var actual = provider.GetMarketIndex(new MarketValue("x"), DateTime.Today);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetMarketIndex_MultipleValues_WithinTolerance()
        {
            double expected1 = 1.0;
            double expected2 = 1.0001;

            var provider = new CompositeMarketIndexProvider(new IMarketValueProvider[]
                                                    {
                                                        new WorkingTestMarketIndexProvider(expected1),
                                                        new WorkingTestMarketIndexProvider(expected2)
                                                    });

            var actual = provider.GetMarketIndex(new MarketValue("x"), DateTime.Today);

            Assert.Equal(expected1, actual, 6);
        }

        [Fact]
        public void GetMarketIndex_MultipleValues_NotWithinTolerance_Throws()
        {
            double expected1 = 1.0;
            double expected2 = 1.01;

            var provider = new CompositeMarketIndexProvider(new IMarketValueProvider[]
                                                    {
                                                        new WorkingTestMarketIndexProvider(expected1),
                                                        new WorkingTestMarketIndexProvider(expected2)
                                                    });

            Assert.Throws<InconsistentMarketValuesException>(() => provider.GetMarketIndex(new MarketValue("x"), DateTime.Today));
        }

        [Fact]
        public void GetMarketIndex_NoValues_Throws()
        {
            var provider = new CompositeMarketIndexProvider(new IMarketValueProvider[]
                                                    {
                                                        new NonWorkingTestMarketIndexProvider()
                                                    });

            var ex = Assert.Throws<MarketIndexNotFoundException>(() => provider.GetMarketIndex(new MarketValue("x"), DateTime.Today));
        }
    }

    internal class WorkingTestMarketIndexProvider : IMarketValueProvider
    {
        readonly double index;

        public WorkingTestMarketIndexProvider(double index)
        {
            this.index = index;
        }

        public double GetMarketIndex(MarketValue marketIndex, DateTime date)
        {
            return index;
        }
    }

    internal class NonWorkingTestMarketIndexProvider : IMarketValueProvider
    {
        public double GetMarketIndex(MarketValue marketIndex, DateTime date)
        {
            throw new Exception();
        }
    }
}
