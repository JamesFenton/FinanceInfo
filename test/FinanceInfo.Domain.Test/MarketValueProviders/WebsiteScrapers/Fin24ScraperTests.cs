﻿using FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers;
using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace FinanceInfo.Domain.Test.MarketValueProviders.WebsiteScrapers
{
    public class Fin24ScraperTests
    { 
        public static IEnumerable<object[]> GetValue_WorkingTestCases()
        {
            foreach (var marketIndex in Fin24Scraper.IndicesLookup.Keys)
            {
                yield return new object[] { marketIndex.Identifier };
            }
        }

        [Theory]
        [MemberData(nameof(GetValue_WorkingTestCases))]
        public void GetValue_Works(string marketIndex)
        {
            var scraper = new Fin24Scraper(new HttpClient());
            var value = scraper.GetMarketIndex(new MarketValue(marketIndex), DateTime.Today);
            Console.WriteLine($"Fetched value {value} for {marketIndex} from {scraper.PageToScrape}");
            Assert.True(value > 0);
        }
    }
}
