﻿using FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers;
using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace FinanceInfo.Domain.Test.MarketValueProviders.WebsiteScrapers
{
    public class ShareDataScraperTests
    {
        public static IEnumerable<object[]> GetValueTestCases()
        {
            foreach (var marketIndex in ShareDataScraper.IndicesLookup.Keys)
            {
                yield return new object[] { marketIndex.Identifier };
            }
        }

        [Theory]
        [MemberData(nameof(GetValueTestCases))]
        public void GetValue_Works(string marketIndex)
        {
            var scraper = new ShareDataScraper(new HttpClient());

            var value = scraper.GetMarketIndex(new MarketValue(marketIndex), DateTime.Today);
            Console.WriteLine($"Fetched value {value} for {marketIndex} from {scraper.PageToScrape}");
            Assert.True(value > 0);
        }
    }
}
