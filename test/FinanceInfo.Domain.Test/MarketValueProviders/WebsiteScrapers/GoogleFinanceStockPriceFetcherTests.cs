﻿using FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers;
using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FinanceInfo.Domain.Test.MarketValueProviders.WebsiteScrapers
{
    public class GoogleFinanceStockPriceFetcherTests
    {
        [Theory]
        [InlineData("OML")]
        [InlineData("CML")]
        public void GetPrice_FetchesPrice_Correctly(string ticker)
        {
            var pricer = new GoogleFinanceScraper(new HttpClient());
            var price = pricer.GetMarketIndex(new MarketValue(ticker), DateTime.Today);
            Console.WriteLine($"Fetched price {price} for {ticker} from http://www.google.com/finance");
            Assert.True(price > 0);
        }

        [Fact]
        public void GetPrice_Yesterday_ThrowsException()
        {
            var marketIndex = new MarketValue("OML");
            var date = DateTime.Today.AddDays(-1);
            var pricer = new GoogleFinanceScraper(new HttpClient());

            var ex = Assert.Throws<ArgumentException>(() => pricer.GetMarketIndex(marketIndex, date));
        }
        
    }
}
