﻿using FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers;
using System;
using System.Collections.Generic;
using System.Linq;
using FinanceInfo.Models;
using Xunit;
using System.Net.Http;
using FinanceInfo.Domain.MarketValueProviders;

namespace FinanceInfo.Domain.Test.MarketValueProviders
{
    public class CompositeMarketIndexProviderIntTests
    {
        public static IEnumerable<object[]> GetMarketIndexData()
        {
            yield return new[] { MarketValues.JSE.AllShare.Identifier };
            yield return new[] { MarketValues.JSE.Resource10.Identifier };
            yield return new[] { MarketValues.JSE.Industrial25.Identifier };
            yield return new[] { MarketValues.JSE.Financial15.Identifier };
        }

        [Theory]
        [MemberData(nameof(GetMarketIndexData))]
        public void GetMarketIndex(string index)
        {
            var compositeProvider = new CompositeMarketIndexProvider(new List<IMarketValueProvider>
            {
                new Fin24Scraper(new HttpClient()),
                new ShareDataScraper(new HttpClient())
            });
            compositeProvider._Tolerance = 100; // test tolerance to account for delays in values

            var actual = compositeProvider.GetMarketIndex(new MarketValue(index), DateTime.Today);

            Assert.True(actual > 0);
        }
    }
}
