﻿using FinanceInfo.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace FinanceInfo.Domain.Test.Helpers
{
    public class MathHelpersTests
    {
        public static IEnumerable<object[]> RangeTestData()
        {
            yield return new object[] { new double[] { 2, 1 }, 1 };
            yield return new object[] { new double[] { 5, 2, 3 }, 3 };
            yield return new object[] { new double[] { 1, 2, 3, 4, -1 }, 5 };
        }

        [Theory]
        [MemberData(nameof(RangeTestData))]
        public void RangeTests(double[] points, double expected)
        {
            var actual = MathHelpers.Range(points);

            Assert.Equal(expected, actual);
        }
    }
}
