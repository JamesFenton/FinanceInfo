﻿using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers
{
    /// <summary>
    /// An object which fetches stock prices
    /// </summary>
    public class GoogleFinanceScraper : IMarketValueProvider
    {
        /// <summary>
        /// The http client to fetch data with
        /// </summary>
        readonly HttpClient _httpClient;

        /// <summary>
        /// Creates a stock price fetcher with the given <paramref name="httpClient"/>.
        /// </summary>
        /// <param name="httpClient">The http client to fetch data with</param>
        public GoogleFinanceScraper(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Gets the price (in ZAR) from Google finance of the stock with specified <paramref name="ticker"/>.
        /// NB: this only handles today's stock price.
        /// </summary>
        /// <param name="ticker">The market index to fetch.</param>
        /// <param name="date">The date on which to fetch the price. NB: must be today.</param>
        /// <returns>The current price in ZAR of the stock</returns>
        public double GetMarketIndex(MarketValue marketIndex, DateTime date)
        {
            if (date.Date != DateTime.Today)
                throw new ArgumentException("Can only provide a market value for today");

            var page = _httpClient.GetStringAsync(String.Format("https://www.google.com/finance?q=JSE:{0}", marketIndex.Identifier)).Result;
            var matches = Regex.Matches(page, "ref_(.*?)>(.*?)<");
            var match = matches[0];
            var group = match.Groups[2];
            var value = group.Value;
            var price = Double.Parse(value.Replace(",", "")) / 100.0;
            return price;
        }

        public override string ToString()
        {
            return nameof(GoogleFinanceScraper);
        }
    }
}