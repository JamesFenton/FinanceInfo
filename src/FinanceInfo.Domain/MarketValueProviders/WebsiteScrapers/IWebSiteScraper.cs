﻿using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers
{
    public interface IWebSiteScraper : IMarketValueProvider
    {
        /// <summary>
        /// The web page to scrape
        /// </summary>
        string PageToScrape { get; }

        /// <summary>
        /// The market indices that this website can provide.
        /// </summary>
        IEnumerable<MarketValue> AvailableIndices { get; }
    }
}
