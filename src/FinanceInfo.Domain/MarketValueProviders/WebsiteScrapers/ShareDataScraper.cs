﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceInfo.Models;
using System.Net.Http;
using FinanceInfo.Domain.Exceptions;
using System.Text.RegularExpressions;

namespace FinanceInfo.Domain.MarketValueProviders.WebsiteScrapers
{
    public class ShareDataScraper : IWebSiteScraper
    {
        /// <summary>
        /// The web page to scrape
        /// </summary>
        public string PageToScrape => "http://www.sharedata.co.za/v2/scripts/Home.aspx";

        /// <summary>
        /// The market indices that this website can provide.
        /// </summary>
        public IEnumerable<MarketValue> AvailableIndices
        {
            get
            {
                return IndicesLookup.Keys.ToList();
            }
        }

        internal static Dictionary<MarketValue, string> IndicesLookup = new Dictionary<MarketValue, string>
                {
                    // local
                    {MarketValues.JSE.AllShare, "All Share"},
                    { MarketValues.JSE.Resource10, "Resource 10"},
                    {MarketValues.JSE.Industrial25, "Industrial 25"},
                    {MarketValues.JSE.Financial15, "Financial 15"}
                };

        private readonly HttpClient _httpClient;

        /// <summary>
        /// Creates a new instance to fetch values from the website.
        /// </summary>
        /// <param name="httpClient">The http client used to fetch the website.</param>
        public ShareDataScraper(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Gets a particular value from the website.
        /// </summary>
        /// <param name="marketIndex">The market index to fetch</param>
        /// <param name="date">The date to fetch the value for</param>
        /// <returns>The value.</returns>
        public double GetMarketIndex(MarketValue marketIndex, DateTime date)
        {
            if (!IndicesLookup.ContainsKey(marketIndex))
                throw new ArgumentException($"{nameof(Fin24Scraper)} does not provide a market value for {marketIndex}");
            if (date.Date != DateTime.Today)
                throw new ArgumentException("Can only provide a market value for today");

            var page = _httpClient.GetStringAsync(PageToScrape).Result;
            var tokenString = IndicesLookup[marketIndex];

            var htmlSnippet = GetHtmlSnippetFromTokenToPrice(page, tokenString);
            if (String.IsNullOrEmpty(htmlSnippet))
                throw new WebsiteScrapingException(PageToScrape, tokenString);

            var numberToParse = GetNumberFromHtml(htmlSnippet);
            if (String.IsNullOrEmpty(numberToParse))
                throw new WebsiteScrapingException(PageToScrape, tokenString);

            double value;
            Double.TryParse(numberToParse, out value);
            if (value == default(double))
                throw new WebsiteScrapingException(PageToScrape, tokenString);

            return value;
        }

        /// <summary>
        /// eg if token = "All Share", will match:
        /// "All Share [anything]  >123.45<
        /// </summary>
        /// <param name="html"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        internal string GetHtmlSnippetFromTokenToPrice(string html, string token)
        {
            Regex r = new Regex($">{token}.*[0123456789 ]");
            var match = r.Match(html).Value;

            if (String.IsNullOrEmpty(match))
                throw new WebsiteScrapingException(PageToScrape, token);
            return match;
        }

        /// <summary>
        /// Extracts the first double matching >1234,65<
        /// </summary>
        /// <param name="html">The html to search in</param>
        /// <returns></returns>
        internal string GetNumberFromHtml(string html)
        {
            Regex r = new Regex(@">( *\d* \d+)<");
            var matches = r.Matches(html);
            var match = matches[0].Value;
            var number = match.Replace(",", ".").Replace(">", "").Replace("<", "").Replace(" ", "");
            return number;
        }

        public override string ToString()
        {
            return nameof(ShareDataScraper);
        }
    }
}
