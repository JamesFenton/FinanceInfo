﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceInfo.Models;
using FinanceInfo.Domain.MarketValueProviders;
using FinanceInfo.Domain.Exceptions;
using FinanceInfo.Domain.Helpers;

namespace FinanceInfo.Domain.MarketValueProviders
{
    public class CompositeMarketIndexProvider : IMarketValueProvider
    {
        private readonly List<IMarketValueProvider> _providers;

        internal double _Tolerance = 1e-3;

        public CompositeMarketIndexProvider(IEnumerable<IMarketValueProvider> providers)
        {
            _providers = providers.ToList();
        }

        public double GetMarketIndex(MarketValue marketIndex, DateTime date)
        {
            var results = new Dictionary<IMarketValueProvider, double>();

            foreach (var provider in _providers)
            {
                try
                {
                    results[provider] = provider.GetMarketIndex(marketIndex, date);
                }
                catch {}
            }

            double value = VerifyMarketIndicesFromDifferentProviders(results, marketIndex, date);
            return value;
        }

        internal double VerifyMarketIndicesFromDifferentProviders(Dictionary<IMarketValueProvider, double> results, MarketValue marketIndex, DateTime date)
        {
            if (!results.Any())
                throw new MarketIndexNotFoundException(marketIndex, date, _providers.ToArray());
            
            double differece = MathHelpers.Range(results.Values);
            if (differece > _Tolerance)
                throw new InconsistentMarketValuesException(results, marketIndex, date);

            return results.First().Value;
        }
    }
}
