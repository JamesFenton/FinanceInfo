﻿using FinanceInfo.Models;
using System;

namespace FinanceInfo.Domain.MarketValueProviders
{
    /// <summary>
    /// An interface for fetching stock prices
    /// </summary>
    public interface IMarketValueProvider
    {
        /// <summary>
        /// Gets a market index value.
        /// </summary>
        /// <param name="marketIndex">The market index to get. See <see cref="MarketValues"/></param>
        /// <param name="date">The date on which to fetch the index.</param>
        /// <returns>The value of the index on the specified date</returns>
        double GetMarketIndex(MarketValue marketIndex, DateTime date);
    }
}