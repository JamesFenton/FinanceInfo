﻿using FinanceInfo.Domain.Helpers;
using FinanceInfo.Domain.MarketValueProviders;
using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Domain.Exceptions
{
    public class MarketIndexNotFoundException : Exception
    {
        public MarketIndexNotFoundException(string message) : base(message)
        {
        }

        readonly MarketValue marketIndex;
        readonly DateTime date;
        readonly IMarketValueProvider[] providers;

        public MarketIndexNotFoundException(MarketValue marketIndex, DateTime date, params IMarketValueProvider[] providers)
        {
            this.marketIndex = marketIndex;
            this.date = date;
            this.providers = providers;
        }

        public override string Message
        {
            get
            {
                return $"Could not find index {marketIndex} on {date.ToIsoDateFormat()} from providers: {String.Join(", ", providers.Select(p => p.GetType().Name))}";
            }
        }
    }
}
