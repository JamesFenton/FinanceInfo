﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FinanceInfo.Domain.Exceptions
{
    public class WebsiteScrapingException : Exception
    {
        public WebsiteScrapingException(string website, string token) : base(String.Format("Could not find value for {0} on {1}", token, website))
        {
        }
    }
}