﻿using FinanceInfo.Domain.Helpers;
using FinanceInfo.Domain.MarketValueProviders;
using FinanceInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Domain.Exceptions
{
    public class InconsistentMarketValuesException : Exception
    {
        public InconsistentMarketValuesException(string message) : base(message)
        {
        }

        public readonly Dictionary<IMarketValueProvider, double> Results;
        public readonly MarketValue MarketIndex;
        public readonly DateTime Date;

        public InconsistentMarketValuesException(Dictionary<IMarketValueProvider, double> results, MarketValue marketIndex, DateTime date)
        {
            Results = results;
            MarketIndex = marketIndex;
            Date = date;
        }

        public override string Message
        {
            get
            {
                return $"Got conflicting values from different market index providers when fetching index {MarketIndex} on {Date.ToIsoDateFormat()}: {String.Join(", ", Results)}";
            }
        }
    }
}
