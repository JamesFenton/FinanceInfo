﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Domain.Helpers
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Returns a date formatted as yyyy-MM-dd
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToIsoDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}
