﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Domain.Helpers
{
    public static class MathHelpers
    {
        /// <summary>
        /// Calculates the standard deviation of a set of points
        /// </summary>
        /// <param name="points">The points</param>
        /// <returns></returns>
        public static double StdDev(IEnumerable<double> points)
        {
            double average = points.Average();
            double sumOfSquaresOfDifferences = points.Select(val => (val - average) * (val - average)).Sum();
            return Math.Sqrt(sumOfSquaresOfDifferences / points.Count());
        }

        /// <summary>
        /// Calculates the coefficient of variation of a set of points 
        /// </summary>
        /// <param name="points">The points</param>
        /// <returns></returns>
        public static double CoefficientOfVariation(IEnumerable<double> points)
        {
            double stdDev = StdDev(points);
            double average = points.Average();
            return stdDev / average;
        }

        /// <summary>
        /// Calculates the difference between the max and the min of a set of points
        /// </summary>
        /// <param name="points">The points</param>
        /// <returns></returns>
        public static double Range(IEnumerable<double> points)
        {
            var orderedPoints = points.OrderBy(x => x);
            return orderedPoints.Last() - orderedPoints.First();
        }
    }
}
