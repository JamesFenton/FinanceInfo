﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Models
{
    public class MarketValue
    {
        public string Identifier { get; private set; }

        public MarketValue(string identifier)
        {
            Identifier = identifier;
        }

        public override bool Equals(object obj)
        {
            MarketValue casted = obj as MarketValue;
            if (casted == null)
                return false;

            return Identifier == casted.Identifier;
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }

        public override string ToString()
        {
            return Identifier;
        }
    }
}
