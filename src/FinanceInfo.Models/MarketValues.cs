﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceInfo.Models
{
    public static class MarketValues
    {
        /// <summary>
        /// Johannesburg Stock Exchange
        /// </summary>
        public static class JSE
        {
            public static MarketValue AllShare = new MarketValue("ALSI");
            public static MarketValue Top40 = new MarketValue("Top40");
            public static MarketValue Financial15 = new MarketValue("Financial15");
            public static MarketValue Industrial25 = new MarketValue("Industrial25");
            public static MarketValue Resource10 = new MarketValue("Resource10");
        }

        /// <summary>
        /// London Stock Exchange
        /// </summary>
        public static class LSE
        {
            public static MarketValue Ftse100 = new MarketValue("FTSE100");
        }

        /// <summary>
        /// Non exchange specific indices in the USA
        /// </summary>
        public static class USA
        {
            public static MarketValue DowJonesIndustrial = new MarketValue("DJI");
        }

        /// <summary>
        /// Toyko Stock Exchange
        /// </summary>
        public static class TSE
        {
            public static MarketValue Nikkei225 = new MarketValue("N225");
        }

        /// <summary>
        /// Shanghai Stock Exchange
        /// </summary>
        public static class SSE
        {
            public static MarketValue SseComposite = new MarketValue("SHCOMP");
        }

        /// <summary>
        /// Currency Exchange Rates
        /// </summary>
        public static class Currencies
        {
            public static MarketValue UsdZar = new MarketValue("USD/ZAR");
            public static MarketValue GbpZar = new MarketValue("GBP/ZAR");
            public static MarketValue EurZar = new MarketValue("EUR/ZAR");
            public static MarketValue JpyZar = new MarketValue("JPY/ZAR");
            public static MarketValue AudZar = new MarketValue("AUD/ZAR");
        }

        /// <summary>
        /// Commodities
        /// </summary>
        public static class Commodities
        {
            public static MarketValue Gold = new MarketValue("Gold");
            public static MarketValue Platinum = new MarketValue("Platinum");
            public static MarketValue Silver = new MarketValue("Silver");
            public static MarketValue BrentCrudeOil = new MarketValue("Oil");
        }
    }
}
