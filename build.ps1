$projects = @("src\FinanceInfo.Data", "src\FinanceInfo.Domain", "src\FinanceInfo.Models", "src\FinanceInfo.Web")
$testProjects = @("test\FinanceInfo.Domain.Test")
$allProjects = $projects + $testProjects
$dnvmVersion = "1.0.0-rc1-update1"

# dnvm version
Write-Host "== DNVM Install =="
dnvm install $dnvmVersion -r coreclr
dnvm use $dnvmVersion -r coreclr

# Nuget Restore
Write-Host "== Nuget Restore =="
dnu restore $allProjects --quiet
if ($LastExitCode -ne 0) {exit $LastExitCode}

# Build
Write-Host "== Build =="
$configuration = "Release"
dnu build --quiet --configuration $configuration $allProjects 
if ($LastExitCode -ne 0) {exit $LastExitCode}

# Test
Write-Host "== Test =="
foreach($testProject in $testProjects) {
	dnx -p $testProject test
}
if ($LastExitCode -ne 0) {exit $LastExitCode}